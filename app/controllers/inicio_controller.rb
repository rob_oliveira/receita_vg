class InicioController < ApplicationController
  def index
  	if usuario_signed_in?
  		@current = current_usuario
  	elsif auditor_signed_in?
  		@current = current_auditor
  	end
  end
end
