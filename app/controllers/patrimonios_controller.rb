class PatrimoniosController < ApplicationController
  before_action :set_patrimonio, only: [:show, :edit, :update, :destroy]


  # GET /patrimonios
  # GET /patrimonios.json
  def index
    @patrimonios = Patrimonio.all
  end

  # GET /patrimonios/1
  # GET /patrimonios/1.json
  def show
  end

  # GET /patrimonios/new
  def new
    @patrimonio = Patrimonio.new
  end

  # GET /patrimonios/1/edit
  def edit
  end

  # POST /patrimonios
  # POST /patrimonios.json
  def create
    @patrimonio = Patrimonios.new(patrimonio_params)

    respond_to do |format|
      if @patrimonio.save
        puts "salvo"
        format.html { redirect_to @patrimonio, notice: 'Patrimonio was successfully created.' }
        format.json { render :show, status: :created, location: @patrimonio }
      else
        puts @patrimonio.errors.full_messages
        format.html { render :new }
        format.json { render json: @patrimonio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patrimonios/1
  # PATCH/PUT /patrimonios/1.json
  def update
    respond_to do |format|
      if @patrimonio.update(patrimonio_params)
        format.html { redirect_to @patrimonio, notice: 'Patrimonio was successfully updated.' }
        format.json { render :show, status: :ok, location: @patrimonio }
      else
        format.html { render :edit }
        format.json { render json: @patrimonio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patrimonios/1
  # DELETE /patrimonios/1.json
  def destroy
    @patrimonio.destroy
    respond_to do |format|
      format.html { redirect_to patrimonios_url, notice: 'Patrimonio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patrimonio
      @patrimonio = Patrimonio.find(params[:id])
      #@usuario = Usuario.find(@patrimonio.usuario_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patrimonio_params
      params.require(:patrimonio).permit(:nome_patrimonio, :tipo, :valor, :usuario_id)
    end


end
