class Usuario < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

#	devise :registerable, :confirmable

	has_many :patrimonios, inverse_of: :usuario

	accepts_nested_attributes_for :patrimonios, allow_destroy: true

#	validates :nome, presence: true
#	validates :sexo, presence: true
#	validates :idade, presence: true, 	numericality: {grater_than: 0, only_integer: true}
#	validates :endereco, presence: true
#	validates :cidade, presence: true

end
