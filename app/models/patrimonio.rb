class Patrimonio < ApplicationRecord

	belongs_to :usuario

	validates :nome_patrimonio, presence: true
	validates :tipo, presence: true
	validates :valor, presence: true

end
