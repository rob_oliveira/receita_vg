class CreatePatrimonios < ActiveRecord::Migration[5.0]
  def change
    create_table :patrimonios do |t|
      t.string :nome_patrimonio
      t.string :tipo
      t.float :valor
      t.integer :usuario_id

      t.timestamps
    end
  end
end
