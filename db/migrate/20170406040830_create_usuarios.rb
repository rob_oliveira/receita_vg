class CreateUsuarios < ActiveRecord::Migration[5.0]
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.string :sexo
      t.integer :idade
      t.string :endereco
      t.string :cidade
      #t.patrimonio :patrimonios

      t.timestamps
    end
  end
end
