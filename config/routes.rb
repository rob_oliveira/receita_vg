Rails.application.routes.draw do
  
  devise_for :usuarios, path: 'u'
  devise_for :auditors, path: 'a'
  
  get 	'inicio/index'
  get 	'/usuarios-cidade/:cidade' =>'usuarios#usuarios_cidade'
  get 	'/usuarios-alta-receita' =>'usuarios#usuarios_alto_valor'

  root :to => "inicio#index" 

  resources :patrimonios
  resources :usuarios
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
